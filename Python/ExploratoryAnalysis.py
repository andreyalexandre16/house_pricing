import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

train_dt = pd.read_csv('/home/andrey/Projetos/house_pricing/Data/train.csv')
test_dt = pd.read_csv('/home/andrey/Projetos/house_pricing/Data/test.csv')

train_dt.describe()

sns.FacetGrid(train_dt)


X_train = train_dt.drop(train_dt.columns[[0,80]], axis = 1)
y_train = train_dt.iloc[:, 80]

X_test = test_dt.drop(test_dt.columns[[0,80]], axis = 1)
y_test = test_dt.iloc[:, 80]

